# DataCashPage

### The objective

Create a HTML page for a card payment form which is responsive to different view ports and contains only inline JavaScript, Styles and images.

The file will be generated here:

    dist/index.html

This then needs to be passed to ODEON for them to upload to DataCash.

### How to install

Clone the project:

    git clone https://gitlab.com/matthewfedak/datacashpage.git

Inside the project root run:

    npm install


### Run the local environment

To run simply use:

     gulp

The page will be viewable here `http://localhost:3000/`

The browser will be reloaded after any changes are made to the
source files.