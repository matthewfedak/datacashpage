/**
* Use validate.js library
*/
var validate = require("validate.js");

/**
 * Validation constraints
 */
var validationConstraints = {
    capf1: {
        presence: true
    },
    card_number: {
        presence: true,
        format: {
            pattern: /^(34|37|4|5[1-5]).*$/,
            message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                return validate.format("^%{num} is not a valid credit card number", {
                    num: value
                });
            }
        }
    },
    cv2_number: {
        presence: true,
        format: {
            pattern: /^[0-9]{3,4}$/,
            message: function(value) {
                return validate.format("^%{num} is not a valid card security Code", {
                    num: value
                });
            }
        }
    },
    issue_number: {
        presence: false,
        format: {
            pattern: /^[0-9]{1,2}$/,
            message: function(value) {
                return validate.format("^%{num} is not a valid issue number", {
                    num: value
                });
            }
        }
    },
    exp_month: {
        presence: true,
        format: {
            pattern: /^(1[0-2]|[1-9])$/,
            message: function() {
                return "Expiry month is not valid";
            }

        }
    },
    exp_year: {
        presence: true,
        format: {
            pattern: /^(20)\d{2}$/,
            message: function() {
                return "Expiry year is not valid";
            }
        }
    }
};

/**
 * Setup tooltip
 */
function setupToolTip() {

    /**
     * Find all anchors with class help
     */
    var $helpAnchors = document.querySelectorAll('a.help');
    if ($helpAnchors.length < 1) {
        return;
    }

    /**
     * Get tooltip
     */
    var $toolTip = document.getElementById('tooltip');
    var $toolTipBody = document.getElementById('helpfulText');

    /**
     * Attach event listeners to help anchors
     */
    for (var i = 0; i < $helpAnchors.length; i++) {

        var $helpAnchor = $helpAnchors[i];

        $helpAnchor.addEventListener('click', function (event) {
            event.preventDefault();
        });

        $helpAnchor.addEventListener('mouseover', function () {

            $toolTip.style.display = 'block';
            $toolTipBody.innerHTML = $helpAnchor.getAttribute('title');

            $helpAnchor.setAttribute('title', '');

            /**
             * @Todo fix this later
             */
            /*$(this).position().top - $('#tooltip').outerHeight() + "px";*/
            /*$(this).position().left - 5 + "px";*/

            $toolTip.style.top = '0px';
            $toolTip.style.left = '0px';
            $toolTip.style.display = 'block';

        });

        $helpAnchor.addEventListener('mouseout', function () {

            $toolTip.style.display = 'none';
            $helpAnchor.setAttribute('title', $toolTipBody.innerHTML);

        });
    }
}

/**
 * Read DataCash Errors hidden in DOM and move to another element
 * - These are populated via server side validation
 */
function handleDataCashErrors() {

    var aErrors = document.querySelectorAll('p.error_message');
    var $errorPage = document.getElementById('errorpage');

    if (aErrors.length < 1) {
        return;
    }

    var $errorNode = document.createElement('span');
    $errorNode.setAttribute('generated', 'true');
    $errorNode.setAttribute('class', 'field-error');

    for (var i = 0; i < aErrors.length; i++) {
        $errorNode.innerHTML = aErrors[i].innerHTML.trim();
        $errorPage.appendChild($errorNode);
    }

    $errorPage.style.display = 'block';
}

/**
 * Handle form submit
 *
 * @param form
 * @param input
 */
function handleFormSubmit(form, input) {
    /**
     * Run the form validation in submit
     */
    var errors = validate(form, validationConstraints);

    /**
     * SHow any errors in the error div
     */
    showErrors(form, errors || {});

    /**
     * If no errors submit the form
     */
    if (!errors) {
        document.getElementById('paymentDetails').submit();
    }
}

/**
 * Updates the inputs with the validation errors
 *
 * @param form
 * @param errors
 */
function showErrors(form, errors) {
    var $inputs = document.querySelectorAll('input[name], select[name]');
    for (var i = 0;i < $inputs.length;i++){
        var $input = $inputs[i];
        showErrorsForInput($input, errors && errors[$input.name]);
    }
}

/**
 * Shows the errors for a specific input
 *
 * @param input
 * @param errors
 */
function showErrorsForInput(input, errors) {
    input.classList.remove('field-error');
    if (errors) {
        input.classList.add('field-error');
    }
}

/**
 * Run when page is ready
 */
window.addEventListener('load', function () {

    /**
     * Setup tooltip
     */
    setupToolTip();

    /**
     * Hide any DataCash errors
     */
    var dataCashError = document.getElementById('datacash-error');
    dataCashError.style.display = 'none';

    /**
     * Handle any DataCash errors
     */
    handleDataCashErrors();

    /**
     * Prevent form being posted
     */
    var form = document.querySelector("form#paymentDetails");
    form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
    });

    /**
     * Add event listeners for on the fly validation
     */
    var inputs = document.querySelectorAll("input, select")
    for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
            var errors = validate(form, validationConstraints) || {};
            showErrorsForInput(this, errors[this.name]);
        });
    }
});

