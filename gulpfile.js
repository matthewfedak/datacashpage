/**
 * Requirements
 */
const { src, dest, series, task } = require('gulp');
const sass                        = require('gulp-sass');
const concat                      = require('gulp-concat');
const rm                          = require('gulp-rm');
const watch                       = require('gulp-watch');
const uglify                      = require('gulp-uglify');
const tap                         = require('gulp-tap');
const inlineSource                = require('gulp-inline-source');
const browserify                  = require('browserify');
const browserSync                 = require('browser-sync').create();
const reload                      = browserSync.reload;

/**
 * Create a mini Server and watch for source file changes
 */
function serve() {

    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    watch(
        [
            './src/**/*'
        ],
        series('clean', 'compileSass', 'browserifyJS', 'concatJS', 'compressJS', 'injectInlineAssets')
    );

    return watch(['./src/**/*']).on('change', reload);
}

/**
 * Convert JS required
 */
function browserifyJS()
{
    return src('src/js/**/*', {read: false})
        .pipe(tap(function (file) {
            file.contents = browserify(file.path, {debug: true}).bundle();
        }))
        .pipe(dest('build'));
}

/**
 * Compress all SCSS files to one css file
 */
function compileSass() {
    return src('./src/scss/main.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(dest('./build'));
}

/**
 * Remove old js files
 */
function clean() {
    return src('./build/**/*', { read: false })
        .pipe(rm())
}

/**
 * Concat all JS files
 */
function concatJS() {
    return src('./build/**/*.js', { sourcemaps: true })
        .pipe(concat('page.js'))
        .pipe(dest('build', { sourcemaps: true }))
}

/**
 * Compress concatenated JS
 */
function compressJS() {
    return src('./build/page.js')
        .pipe(uglify())
        .pipe(dest('./build'));
}

/**
 * Inject CSS / JS into the final HTML
 */
function injectInlineAssets() {
    return src('./src/index.html')
        .pipe(inlineSource({
            compress: false
        }))
        .pipe(dest('./dist'));
}

task('default', series(clean, compileSass, browserifyJS, concatJS, compressJS, injectInlineAssets, serve));

exports.serve = serve;
exports.compileSass = compileSass;
exports.clean = clean;
exports.concatJS = concatJS;
exports.browserifyJS = browserifyJS;
exports.compressJS = compressJS;
exports.injectInlineAssets = injectInlineAssets;